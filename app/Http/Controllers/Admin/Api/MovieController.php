<?php

namespace App\Http\Controllers\Admin\Api;

use App\Movie;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class MovieController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    /**
     * Create a movie
     *
     * @param UserRegisterRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'imdb_id' => 'max:10|regex:[tt\\d{7}]',
            'title' => 'required|max:191',
            'description' => 'required',
            'length' => 'required|numeric|between:0.01,999.99',
            'rating' => 'numeric|min:1',
            'released_date' => 'required|date|date|date_format:Y-m-d',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $movie = Movie::create($input);
        $success['id'] = $movie->id;

        return response()->json(['success' => $success], 200);
    }
}
