<?php

namespace App\Http\Controllers\Api;

use App\Movie;
use App\Http\Resources\MovieCollection;
use App\Http\Resources\MovieResource;
use App\Http\Controllers\Controller;
use App\Search\MovieSearch;
use Illuminate\Http\Request;

class MovieController extends Controller
{

    /**
     * Create a new MovieController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get all movies
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function searchMovie(Request $request)
    {
        $query = $request->get('query');
        $query = json_decode($query ,true);

        $movieSearch = new MovieSearch();
        $movies = new MovieCollection($movieSearch->apply($request));

        return response()->json(['success' => $movies], 200);
    }

    /**
     * Get a movie
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function getMovie(Request $request, $id)
    {
        $movie = new MovieResource(Movie::findOrFail($id));

        return response()->json(['success' => $movie], 200);
    }
}
