<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    /**
     * Register User
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:191',
            'last_name' => 'required|max:191',
            'email' => 'required|email|max:191|unique:users,email',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();

        $user = User::create($input);

        $success['full_name'] = $user->first_name . ' ' . $user->last_name;
        $success['email'] = $user->email;
        $success['api_key'] = $user->api_key;

        return response()->json(['success' => $success], 200);
    }

    /** Get user's details
     *
     * @return \Illuminate\Http\Response
     */
    public function getDetails()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], 200);
    }

    // Methods below that start with auth-* are used only for testing purposes

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthCheck(Request $request) {
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::check() ? Auth::id() : -9999,
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthUser(Request $request) {
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::user()->id,
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthId(Request $request) {
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthAttempt(Request $request) {
        Auth::attempt([ 'email' => $request->get('email'), 'password' => $request->get('password') ]);
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthOnce(Request $request) {
        Auth::once([ 'email' => $request->get('email'), 'password' => $request->get('password') ]);
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return boolen
     */
    public function tokenAuthLogin(Request $request) {
        Auth::login(User::find($request->get('id')));
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthLoginUsingId(Request $request) {
        Auth::loginUsingId($request->get('id'));
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthLogout(Request $request) {
        Auth::logout();
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function tokenAuthValidate(Request $request) {
        $isValidated = Auth::validate([ 'email' => $request->get('email'), 'password' => $request->get('password') ]);
        return [
            'request' => $request->get('id'),
            'auth'    => $isValidated ? $request->get('id') : -8998,
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function apiAuthCheck(Request $request) {
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::check() ? Auth::id() : -9999,
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function apiAuthUser(Request $request) {
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::user()->id,
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function apiAuthId(Request $request) {
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function apiAuthAttempt(Request $request) {
        Auth::attempt([ 'email' => $request->get('email'), 'password' => $request->get('password') ]);
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function apiAuthOnce(Request $request) {
        Auth::once([ 'email' => $request->get('email'), 'password' => $request->get('password') ]);
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return boolean
     */
    public function apiAuthLogin(Request $request) {
        Auth::login(User::find($request->get('id')));
    }

    public function apiAuthLoginUsingId(Request $request) {
        Auth::loginUsingId($request->get('id'));
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function apiAuthLogout(Request $request) {
        Auth::logout();
        return [
            'request' => $request->get('id'),
            'auth'    => Auth::id(),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function apiAuthValidate(Request $request) {
        $isValidated = Auth::validate([ 'email' => $request->get('email'), 'password' => $request->get('password') ]);
        return [
            'request' => $request->get('id'),
            'auth'    => $isValidated ? $request->get('id') : -8998,
        ];
    }
}
