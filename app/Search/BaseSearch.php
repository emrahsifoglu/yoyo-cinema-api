<?php

namespace App\Search;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

abstract class BaseSearch
{
    protected $model = '';

    public function apply(Request $filters)
    {
        $model = 'App\\' . $this->model;
        $query = $this->applyDecoratorsFromRequest($filters, (new $model)->newQuery());

        return $this->getResults($query);
    }

    public function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = $this->createFilterDecorator($this->normalizeFilterName($filterName));
            if ($this->isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }

        }
        return $query;
    }

    public function createFilterDecorator($name)
    {
        return '\\App\\Filters\\' . $this->model . '\\' . studly_case($name);
    }

    public function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    public function getResults(Builder $query)
    {
        return $query->get();
    }

    private function normalizeFilterName($filterName) {
        return str_replace('_', '', ucwords($filterName, '_'));
    }
}
