<?php

namespace App\Search;

use App\Movie;
use App\Search\BaseSearch;
use Illuminate\Http\Request;

class MovieSearch extends BaseSearch implements Search
{
    public function __construct()
    {
        $this->model = 'Movie';
    }
}
