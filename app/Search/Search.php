<?php

namespace App\Search;

use Illuminate\Http\Request;

interface Search
{
    public function apply(Request $filters);
}
