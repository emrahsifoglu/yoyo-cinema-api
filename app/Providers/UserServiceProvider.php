<?php

namespace App\Providers;

use App\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class UserServiceProvider extends EloquentUserProvider implements UserProvider
{
	private $user;

	public function __construct(User $user) {
		$this->user = $user;
	}

	public function retrieveById($identifier) {
		return $this->user->find($identifier);
	}

	public function retrieveByToken($identifier, $token) {
	    return $this->user->where('api_key', $token)->first();
	}

	public function updateRememberToken(Authenticatable $user, $token) {
	    // Todo: implement updateRememberToken method
	}

	public function retrieveByCredentials(array $credentials) {
	    // Todo: implement retrieveByCredentials method
	}

	public function validateCredentials(Authenticatable $user, array $credentials) {
        // Todo: implement validateCredentials method
	}
}
