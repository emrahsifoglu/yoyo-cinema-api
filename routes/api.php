<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Routes that start with auth-* are used only for testing purposes
|
*/

Route::post('register', 'Api\AuthController@register');
Route::post('auth-attempt', 'Api\AuthController@apiAuthAttempt');
Route::post('auth-once', 'Api\AuthController@apiAuthOnce');
Route::post('auth-login-using-id', 'Api\AuthController@apiAuthLoginUsingId');
Route::post('auth-validate', 'Api\AuthController@apiAuthValidate');
Route::group(['middleware' => 'auth:api'], function() {
    Route::post('auth-login', 'Api\AuthController@apiAuthLogin');
    Route::post('auth-logout', 'Api\AuthController@apiAuthLogout');
    Route::post('auth-check', 'Api\AuthController@apiAuthCheck');
    Route::post('auth-user', 'Api\AuthController@apiAuthUser');
    Route::post('auth-id', 'Api\AuthController@apiAuthId');
    Route::get('me', 'Api\AuthController@getDetails');
});

Route::get('movie/{id}', 'Api\MovieController@getMovie')->where('id', '[0-9]+');
Route::get('search/movie', 'Api\MovieController@searchMovie');

Route::group(['prefix' => 'admin'], function ($router) {
    Route::post('login', 'Admin\Api\AuthController@login');
    Route::post('logout', 'Admin\Api\AuthController@logout');
    Route::post('refresh', 'Admin\Api\AuthController@refresh');
    Route::get('me', 'Admin\Api\AuthController@getDetails');
    Route::group(['prefix' => 'movie'], function ($router) {
        Route::post('create', 'Admin\Api\MovieController@create');
    });
});
