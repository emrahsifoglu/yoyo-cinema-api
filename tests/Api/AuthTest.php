<?php

namespace Tests\Api;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthTest extends TestCase
{
	use DatabaseMigrations;
	use DatabaseTransactions;

	protected $prefixes = [ 'api' ];
	protected $urls = [
		'auth-attempt',
		'auth-check',
		'auth-id',
		'auth-login-using-id',
		'auth-once',
		'auth-user',
		'auth-validate',
	];
	private $apiKey = '';
	private $email = '';
	private $password = '';
	private $userId = 0;

	public function setUp() {
		parent::setUp();

		factory(User::class, 20)->create();

        $this->userId = rand(1, 20);

        $user = User::find($this->userId);
		$this->email = $user->email;
		$this->password = 'secret';
		$this->apiKey = $user->api_key;
	}

	/**
	 * PROCESS - HOW IT WORKS!
	 * =================================================================================================
	 * ATTEMPT: Tries to validate CREDENTIALS and Attempts to login.
	 * CHECK: Check if the user is logged in or not. For APIs, pass access token - check if user exists.
	 * ID: Get the ID of the currently passed API_KEY
	 * LOGIN USING ID: Get the USER by PASSING the USER ID to AUTH
	 * ONCE: Verify USER CREDENTIALS. For ONCE.
	 * USER: Get the USER from CREDENTIALS/API KEY
	 * VALIDATE: Validate User CREDENTIALS.
	 * =================================================================================================
	 */

    public function testSuccessRegister() {
        $response = $this->post("/api/register", [
            'first_name' => 'first_name',
            'last_name'  => 'last_name',
            'email' => 'user@example.com',
            'password' => $this->password,
            'c_password' => $this->password,
        ]);
        $decoded = $response->decodeResponseJson();
        $success = $decoded['success'];
        $this->assertEquals($success['full_name'], 'first_name last_name');
        $this->assertEquals($success['email'], 'user@example.com');
        $this->assertEquals($response->getStatusCode(), 200);
    }

	public function testSuccessAuthAttempt() {
	    $response = $this->post("/api/auth-attempt", [
			'email'    => $this->email,
			'password' => $this->password,
			'id'       => $this->userId,
		], [ 'Accept' => 'application/json' ]);
		$decoded = $response->decodeResponseJson();
		$this->assertEquals($decoded['request'], $decoded['auth']);
		$this->assertEquals($response->getStatusCode(), 200);
	}

	public function testSuccessAuthCheck() {
	    $response = $this->post("/api/auth-check", [
			'email'    => $this->email,
			'password' => $this->password,
			'id'       => $this->userId,
		], [ 'Accept' => 'application/json', 'Authorization' => "Bearer " . $this->apiKey ]);
		$decoded = $response->decodeResponseJson();
		$this->assertEquals($decoded['request'], $decoded['auth']);
		$this->assertEquals($response->getStatusCode(), 200);
	}

	public function testSuccessAuthId() {
	    $response = $this->post("/api/auth-id", [
			'email'    => $this->email,
			'password' => $this->password,
			'id'       => $this->userId,
		], [ 'Accept' => 'application/json', 'Authorization' => "Bearer " . $this->apiKey ]);
		$decoded = $response->decodeResponseJson();
		$this->assertEquals($decoded['request'], $decoded['auth']);
		$this->assertEquals($response->getStatusCode(), 200);
	}

	public function testSuccessAuthLoginUsingId() {
		$response = $this->post("/api/auth-login-using-id", [
			'email'    => $this->email,
			'password' => $this->password,
			'id'       => $this->userId,
		], [ 'Accept' => 'application/json', 'Authorization' => "Bearer " . $this->apiKey ]);
		$decoded = $response->decodeResponseJson();
		$this->assertEquals($decoded['request'], $decoded['auth']);
		$this->assertEquals($response->getStatusCode(), 200);
	}

	public function testSuccessAuthOnce() {
	    $response = $this->post("/api/auth-once", [
			'email'    => $this->email,
			'password' => $this->password,
			'id'       => $this->userId,
		], [ 'Accept' => 'application/json', 'Authorization' => "Bearer " . $this->apiKey ]);
		$decoded = $response->decodeResponseJson();
		$this->assertEquals($decoded['request'], $decoded['auth']);
		$this->assertEquals($response->getStatusCode(), 200);
	}

	public function testSuccessAuthUser() {
	    $response = $this->post("/api/auth-user", [
			'id' => $this->userId,
		], [ 'Accept' => 'application/json', 'Authorization' => "Bearer " . $this->apiKey ]);
		$decoded = $response->decodeResponseJson();
		$this->assertEquals($decoded['request'], $decoded['auth']);
		$this->assertEquals($response->getStatusCode(), 200);
	}

	public function testSuccessAuthValidate() {
	    $response = $this->post("/api/auth-validate", [
			'email'    => $this->email,
			'password' => $this->password,
			'id'       => $this->userId,
		], [ 'Accept' => 'application/json', 'Authorization' => "Bearer " . $this->apiKey ]);
		$decoded = $response->decodeResponseJson();
		$this->assertEquals($decoded['request'], $decoded['auth']);
		$this->assertEquals($response->getStatusCode(), 200);
	}
}
