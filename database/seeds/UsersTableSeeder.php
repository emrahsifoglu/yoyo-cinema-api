<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the user table seed.
	 *
     * @return void
	 */
	public function run()
    {
        DB::table('users')->delete();

		factory(User::class, 20)->create();
	}
}
